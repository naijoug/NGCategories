//
//  NSAttributedStringSampleController.m
//  NGCategoriesSample
//
//  Created by guojian on 2016/12/12.
//  Copyright © 2016年 http://naijoug.com. All rights reserved.
//

#import "NSAttributedStringSampleController.h"
#import "NSAttributedString+NGExtension.h"

@interface NSAttributedStringSampleController ()

@end

@implementation NSAttributedStringSampleController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
